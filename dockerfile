
FROM nginx:latest

# Set the working directory to /usr/share/nginx/html
WORKDIR /usr/share/nginx/html

# Copy the current directory contents into the container at /usr/share/nginx/html
COPY . /usr/share/nginx/html

# Expose port 80 to the Docker host
EXPOSE 80

# Start nginx when the container launches
CMD ["nginx", "-g", "daemon off;"]



